/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WifiActionsControllerTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void enable_portal() throws Exception {
    }

    @Test
    public void disable_portal() throws Exception {
    }

    @Test
    public void enable_internet() throws Exception {
    }

    @Test
    public void disable_internet() throws Exception {
    }

    @Test
    public void enable_intranet() throws Exception {
    }

    @Test
    public void disable_intranet() throws Exception {
    }

    @Test
    public void add_gateway_port() throws Exception {
    }

    @Test
    public void rem_gateway_port() throws Exception {
    }

    @Test
    public void get_gateway_ports() throws Exception {
    }

}