/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.controller;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by fernando on 7/19/17.
 */
public class WifiUserControllerTest {

    private WifiUserController controller;


    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void login() throws Exception {


    }

    @Test
    public void logout() throws Exception {
    }

    @Test
    public void create_user() throws Exception {

    }

    @Test
    public void delete_user() throws Exception {

    }

    @Test
    public void create_role() throws Exception {
    }

    @Test
    public void delete_role() throws Exception {
    }

    @Test
    public void set_role_default() throws Exception {
    }

    @Test
    public void get_role_default() throws Exception {
    }

    @Test
    public void get_users() throws Exception {
    }

    @Test
    public void get_roles() throws Exception {
    }

    @Test
    public void add_role() throws Exception {
    }

    @Test
    public void rem_role() throws Exception {
    }

}