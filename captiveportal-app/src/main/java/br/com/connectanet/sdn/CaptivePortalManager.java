/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn;

import br.com.connectanet.sdn.model.WifiCaptiveRouter;
import br.com.connectanet.sdn.view.WifiRouterService;
import org.apache.felix.scr.annotations.*;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.Device;
import org.onosproject.net.device.DeviceEvent;
import org.onosproject.net.device.DeviceListener;
import org.onosproject.net.device.DeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Created by fernando on 4/5/17.
 */
@Component(immediate = true)
public class CaptivePortalManager {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected CoreService core_svc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected DeviceService dev_svc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected WifiRouterService wr_svc;

    private InnerDeviceListener dev_ltn = new InnerDeviceListener();

    private ApplicationId appid;

    @Activate
    public void activate(){
        appid = core_svc.registerApplication(getClass().toString());
        dev_svc.addListener(dev_ltn);
        log.info("Starting Captive Portal Application");

    }

    @Deactivate
    public void deactivate(){
        dev_svc.removeListener(dev_ltn);
        log.info("Stopping Captive Portal Application");
    }

    private class InnerDeviceListener implements DeviceListener{
        @Override
        public void event(DeviceEvent event) {
            if (event.type() == DeviceEvent.Type.DEVICE_ADDED || event.type() == DeviceEvent.Type.DEVICE_UPDATED){
                log.info(event.type().name());
                Device dev = event.subject();
                dev_svc.getPorts(dev.id()).forEach( p->{
                    String port_name = p.annotations().value("portName");
                    if (port_name.contains("wlan")){
                        log.info("Wlan found");
                        WifiCaptiveRouter router = WifiCaptiveRouter
                                .builder()
                                .setRouter(dev)
                                .setRouterDescription(Optional.of(dev.manufacturer()))
                                .setWifiRouterPort(p.number())
                                .build();
                        wr_svc.enable_wifi_router(router);
                    }
                });
            }
        }
    }

}
