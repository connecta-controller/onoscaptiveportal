/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.view;

import br.com.connectanet.sdn.model.WifiCaptiveGateway;
import br.com.connectanet.sdn.model.WifiUserPrincipal;
import org.apache.commons.lang3.tuple.Pair;
import org.onosproject.net.Host;
import org.onosproject.net.intent.Key;

import java.util.List;

/**
 * Created by fernando on 09/05/17.
 */
public interface WifiActionsService {

    void enable_portal(WifiUserPrincipal user, Host host);

    void disable_portal(WifiUserPrincipal user);

    void enable_internet(WifiUserPrincipal user, Host host);

    void disable_internet(WifiUserPrincipal user);

    void enable_intranet(WifiUserPrincipal user, Host host);

    void disable_intranet(WifiUserPrincipal user);

    void add_gateway_port(WifiCaptiveGateway gw_port);

    void rem_gateway_port(Key key);

    List<Pair<String, WifiCaptiveGateway>> get_gateway_ports();

}
