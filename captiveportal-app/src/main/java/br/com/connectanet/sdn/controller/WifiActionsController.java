/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.controller;

import br.com.connectanet.sdn.model.WifiCaptiveGateway;
import br.com.connectanet.sdn.model.WifiUserPrincipal;
import br.com.connectanet.sdn.view.WifiActionsService;
import br.com.connectanet.sdn.view.WifiUserService;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.felix.scr.annotations.*;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IpAddress;
import org.onlab.packet.IpPrefix;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.DefaultHost;
import org.onosproject.net.FilteredConnectPoint;
import org.onosproject.net.Host;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.host.HostService;
import org.onosproject.net.intent.Intent;
import org.onosproject.net.intent.IntentService;
import org.onosproject.net.intent.Key;
import org.onosproject.net.intent.PointToPointIntent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static br.com.connectanet.sdn.model.WifiCaptiveGateway.Type;


/**
 * Created by fernando on 08/05/17.
 */
@Component(immediate = true)
@Service
public class WifiActionsController implements WifiActionsService {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private CoreService core_svc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private IntentService intent_svc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private WifiUserService wu_svc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private HostService host_svc;




    private Map<Pair<Key, Key>, Pair<WifiUserPrincipal, Host>> user_portal;
    private Map<Pair<Key, Key>, Pair<WifiUserPrincipal, Host>> user_internet;
    private Map<Pair<Key, Key>, Pair<WifiUserPrincipal, Host>> user_intranet;
    private Map<String, WifiCaptiveGateway> gateway_ports;

    private ApplicationId applicationId;



    @Activate
    public void activate() {
        applicationId = core_svc.registerApplication(getClass().getName());
        user_portal = Collections.synchronizedMap(new HashMap<>());
        user_internet = Collections.synchronizedMap(new HashMap<>());
        user_intranet = Collections.synchronizedMap(new HashMap<>());
        gateway_ports = Collections.synchronizedMap(new HashMap<>());

    }


    @Deactivate
    public void deactivate() {
        user_internet = null;
        user_intranet = null;
    }


    public void enable_captive_portal(String user_id, Host user_host, Host portal_host){

    }

    @Override
    public void enable_portal(WifiUserPrincipal user, Host host) {
        enable_service_network(user, host, Type.portal);
    }

    @Override
    public void disable_portal(WifiUserPrincipal user) {
        disable_service_network(user, Type.portal);
    }

    @Override
    public void enable_internet(WifiUserPrincipal user, Host host) {
        enable_service_network(user, host, Type.internet);
    }

    @Override
    public void disable_internet(WifiUserPrincipal user) {
        disable_service_network(user, Type.internet);
    }

    @Override
    public void enable_intranet(WifiUserPrincipal user, Host host) {
        enable_service_network(user, host, Type.intranet);
    }

    @Override
    public void disable_intranet(WifiUserPrincipal user) {
        disable_service_network(user, Type.intranet);
    }

    @Override
    public void add_gateway_port(WifiCaptiveGateway gw_port) {
        String id = UUID.randomUUID().toString();
        gateway_ports.put(id, gw_port);
    }

    @Override
    public void rem_gateway_port(Key key) {
        if (gateway_ports.containsKey(key)) {
            gateway_ports.remove(key);
        }
    }

    @Override
    public List<Pair<String, WifiCaptiveGateway>> get_gateway_ports() {
        List<Pair<String, WifiCaptiveGateway>> gws = new LinkedList<>();
        gateway_ports.forEach((k, v) -> {
            gws.add(Pair.of(k, v));
        });
        return gws;
    }

    private void disable_service_network(WifiUserPrincipal user, Type type) {
        switch (type) {
            case portal:
                user_portal.forEach((ks, us) -> {
                    if (us.getKey().equals(user)) {
                        remove_p2p_intent(ks.getLeft(), ks.getRight());
                        log.info("The user ({}) has disabled of {} network", user.getName(), type.name());
                    }
                });
                break;

            case internet:
                user_internet.forEach((ks, us) -> {
                    if (us.getKey().equals(user)) {
                        remove_p2p_intent(ks.getLeft(), ks.getRight());
                        log.info("The user ({}) has disabled of {} network", user.getName(), type.name());
                    }
                });
                break;

            case intranet:
                user_intranet.forEach((ks, us) -> {
                    if (us.getKey().equals(user)) {
                        remove_p2p_intent(ks.getLeft(), ks.getRight());
                        log.info("The user ({}) has disabled of {} network", user.getName(), type.name());
                    }
                });
                break;
            default:
                log.info("Service unknown");
        }
    }

    private void enable_service_network(WifiUserPrincipal user, Host host, Type type) {

        gateway_ports.forEach((k, gw) -> {
            if (gw.get_type().equals(type.name())) {

                Key key_in;
                Key key_out;

                IpAddress ip_src = IpAddress.valueOf(host.ipAddresses().stream().findFirst().get().toString());
                IpAddress ip_dst = IpAddress.valueOf(gw.get_ip_portal());

                IpPrefix src = IpPrefix.valueOf(ip_src, 32);
                IpPrefix dst = IpPrefix.valueOf(ip_dst, 32);

                TrafficSelector selector_in = DefaultTrafficSelector
                        .builder()
                        .matchEthType(Ethernet.TYPE_IPV4)
                        .matchIPSrc(src)
                        .matchIPDst(dst)
                        .build();

                TrafficSelector selector_out = DefaultTrafficSelector
                        .builder()
                        .matchEthType(Ethernet.TYPE_IPV4)
                        .matchIPSrc(dst)
                        .matchIPDst(src)
                        .build();

                key_in = install_p2p_intent(gw.get_point(), host.location(), selector_in);
                key_out = install_p2p_intent(host.location(), gw.get_point(), selector_out);

                user_portal.put(Pair.of(key_in, key_out), Pair.of(user, host));
                log.info("The user ({}) has enabled to {}  network service", user.getName(), type.name());
            } else {
                log.info("{} network is not configured", type.name());
            }
        });
    }

    private Key install_p2p_intent(ConnectPoint a, ConnectPoint b, TrafficSelector selector) {

        FilteredConnectPoint point_a = new FilteredConnectPoint(a);
        FilteredConnectPoint point_b = new FilteredConnectPoint(b);

        String id = UUID.randomUUID().toString().replaceAll("-", "");
        Key app_key = Key.of(id, applicationId);

        PointToPointIntent p2p_intent = PointToPointIntent
                .builder()
                .filteredIngressPoint(point_a)
                .filteredEgressPoint(point_b)
                .appId(applicationId)
                .key(app_key)
                .selector(selector)
                .treatment(DefaultTrafficTreatment.emptyTreatment())
                .build();

        intent_svc.submit(p2p_intent);

        return app_key;
    }

    private void remove_p2p_intent(Key key_in, Key key_out) {
        Intent intent_in = intent_svc.getIntent(key_in);
        Intent intent_out = intent_svc.getIntent(key_out);
        intent_svc.withdraw(intent_in);
        intent_svc.withdraw(intent_out);
    }

}
