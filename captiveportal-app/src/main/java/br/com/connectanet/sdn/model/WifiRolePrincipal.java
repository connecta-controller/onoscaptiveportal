/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.model;

import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by fernando on 4/26/17.
 */
public class WifiRolePrincipal {

    private final String name;
    private final String id;
    private final  int priority;

    private WifiRolePrincipal(String name, String id, int priority) {
        this.name = name;
        this.id = id;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public int getPriority() {
        return priority;
    }

    public static class Builder {

        private String id;
        private String name;
        private int priority;

        public Builder () {
            this.id = UUID.randomUUID().toString();
        }

        public Builder setName(String name) {
            this.name = checkNotNull(name,"the role name cannot be null");
            return this;
        }

        public Builder setPriority(int priority) {
            this.priority =checkNotNull( priority,"the priority must have a value");
            return this;
        }

        public WifiRolePrincipal build(){
            return (new WifiRolePrincipal(name,id,priority));
        }
    }
}
