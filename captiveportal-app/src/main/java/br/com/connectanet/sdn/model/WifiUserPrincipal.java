/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.google.common.base.Preconditions.checkNotNull;



/**
 * Created by fernando on 4/20/17.
 */
public class WifiUserPrincipal {

    private final String name;
    private final String id;
    private final String create_time;
    private String last_time;
    private String description;

    private AtomicBoolean session;

    private WifiUserPrincipal(String name, String id, String time, String description) {
        this.name = name;
        this.id = id;
        this.create_time = time;
        this.description = description;
        this.last_time = time;
        this.session = new AtomicBoolean(false);
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getCreateTime() {
        return create_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSession() {
        return session.get();
    }

    public void setSession(boolean session) {
        this.session.getAndSet(session);
    }

    public String getLastTime() {
        return last_time;
    }

    public void setLastTime() {
        this.last_time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public static class Builder {
        private String username;
        private String id;
        private String time;
        private Optional<String> description;

        public Builder() {
            this.id = UUID.randomUUID().toString();
            this.time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
        }

        public Builder username(String username) {
            this.username = checkNotNull(username, "The username cannot be null");
            return this;
        }

        public Builder description(Optional<String> descr) {
            this.description = descr;
            return this;
        }

        public WifiUserPrincipal build() {
            return (new WifiUserPrincipal(username, id, time, description.orElse("Not Available")));
        }
    }
}
