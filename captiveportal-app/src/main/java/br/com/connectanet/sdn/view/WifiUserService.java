/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.view;

import br.com.connectanet.sdn.model.WifiRolePrincipal;
import br.com.connectanet.sdn.model.WifiUserPrincipal;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by fernando on 4/13/17.
 */
public interface WifiUserService {

    void login(String username, String password);

    void logout(String username);

    void create_user(String username, String password, Optional<String> description);

    void delete_user(String username);

    void create_role(String role_name, int priority);

    void delete_role(String role_name);

    void set_role_default(String role_id);

    WifiRolePrincipal get_role_default();

    Map<String, WifiUserPrincipal> get_users();

    Map<String, WifiRolePrincipal> get_roles();

    List<String> get_user_roles(String user_name);

    void add_role(String user_name, String role_name);

    void rem_role(String user_name, String role_name);

}
