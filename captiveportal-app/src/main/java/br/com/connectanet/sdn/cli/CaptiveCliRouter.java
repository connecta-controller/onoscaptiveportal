/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.cli;

import br.com.connectanet.sdn.model.WifiCaptiveRouter;
import br.com.connectanet.sdn.view.WifiRouterService;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.onosproject.cli.AbstractShellCommand;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by fernando on 6/12/17.
 */
@Command(scope = "onos", name = "captive-router", description = " Descrption")
public class CaptiveCliRouter extends AbstractShellCommand {

    private static final String DESC_COMMAND =
            "Actions:\n" +
                    "--serial <serial> add-serial\n" +
                    "--serial <serial> del-serial\n" +
                    "list-serial\n" +
                    "list-router\n";

    @Argument(index = 0, name = "action", description = DESC_COMMAND, required = true, multiValued = false)
    private String command = null;

    @Option(name = "--serial", aliases = "-s")
    private String serial = null;

    @Override
    protected void execute() {
        RouterCommand router_cmd = RouterCommand.fromString(command);

        if (command != null) {
            switch (router_cmd) {
                case ADD_SERIAL:
                    add_serial(serial);
                    break;
                case DEL_SERIAL:
                    del_serial(serial);
                    break;
                case LIST_SERIAL:
                    list_serials();
                    break;
                case LIST_ROUTER:

                    break;
                default:
                    log.info("Command not found");
            }
        }
    }
    private void add_serial(String serial){
        WifiRouterService route_svc = AbstractShellCommand.get(WifiRouterService.class);
        route_svc.set_router_serial(serial);
    }
    private void del_serial(String serial){
        WifiRouterService route_svc = AbstractShellCommand.get(WifiRouterService.class);
        route_svc.del_router_serial(serial);
    }


    private void list_router(){

        String exit = "ID: %s" +
                "Device: %s";

        WifiRouterService route_svc = AbstractShellCommand.get(WifiRouterService.class);

        route_svc.get_routers()
                 .stream()
                 .forEach(router -> {
                     print(exit, router.get_uuid(), router.get_device().id().toString());
                 });



    }
    private void list_serials(){

        String exit = "Serial: %s\n" +
                "Device: %s\n" +
                "Status: %s\n";

        WifiRouterService route_svc = AbstractShellCommand.get(WifiRouterService.class);
        Set<String> serials = route_svc.get_serials();

        for (String s : serials){
            Optional<WifiCaptiveRouter> route =  route_svc.get_router_serial(serial);
            if (route.isPresent()){
                boolean status = route_svc.get_router_status(route.get().get_uuid());
                String route_id = route.get().get_device().id().toString();
                if (status){
                    print(exit, s, route_id, "CONNECTED");
                } else {
                    print(exit, s, route_id, "NOT CONNECTED");
                }
            } else {
                print(exit, s, "N/A", "N/A");
            }
        }
    }
}

