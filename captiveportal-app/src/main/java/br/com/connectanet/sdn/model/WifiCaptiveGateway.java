/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.Device;
import org.onosproject.net.Port;

import java.util.Optional;

/**
 * Created by fernando on 09/05/17.
 */
public class WifiCaptiveGateway {

   public enum Type {
        internet, intranet, portal
    }

    private final Device sw;
    private final Port port;
    private final Type type;
    private final String ip;
    private final ConnectPoint point;

    public WifiCaptiveGateway(Device sw, Port port, Type type, String ip) {
        this.sw = sw;
        this.port = port;
        this.type = type;
        this.ip = ip;
        this.point =  new ConnectPoint(sw.id(), port.number());
    }

    public Device get_device() {
        return sw;
    }

    public Port get_port() {
        return port;
    }

    public String get_type() {
        return type.name();
    }

    public String get_ip_portal (){
        return ip;
    }

    public ConnectPoint get_point() {
        return point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WifiCaptiveGateway)) return false;
        WifiCaptiveGateway that = (WifiCaptiveGateway) o;
        return Objects.equal(sw, that.sw) &&
                Objects.equal(port, that.port) &&
                type == that.type &&
                Objects.equal(ip, that.ip) &&
                Objects.equal(point, that.point);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(sw, port, type, ip, point);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sw", sw)
                .add("port", port)
                .add("type", type)
                .add("ip", ip)
                .add("point", point)
                .toString();
    }

    public static WifiCaptiveGateway.Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Device sw;
        private Port port;
        private Type type;
        private String ip;

        private Builder() { }

        public Builder gw_device(Device sw) {
            this.sw = sw;
            return this;
        }

        public Builder gw_port(Port port) {
            this.port = port;
            return this;
        }

        public Builder gw_type(Type type) {
            this.type = type;
            return this;
        }

        public Builder gw_ip_portal(Optional<String> ip_portal){
            this.ip = ip_portal.orElse("not available");
            return this;
        }

        public WifiCaptiveGateway build() {
            return new WifiCaptiveGateway(sw, port, type, ip);
        }
    }
}