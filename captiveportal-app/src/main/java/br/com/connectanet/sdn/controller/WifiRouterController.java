/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.controller;

import br.com.connectanet.sdn.model.WifiCaptiveRouter;
import br.com.connectanet.sdn.view.WifiRouterService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.PortNumber;
import org.onosproject.openflow.controller.Dpid;
import org.onosproject.openflow.controller.OpenFlowController;
import org.onosproject.openflow.controller.OpenFlowSwitch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


/**
 * Created by fernando on 4/5/17.
 */
@Component(immediate = true)
@Service
public class WifiRouterController implements WifiRouterService {

    protected Logger log = LoggerFactory.getLogger(getClass());
    private ConcurrentMap<String, Boolean> router_status;
    private Set<WifiCaptiveRouter> routers;
    private Set<String> serials;


    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected OpenFlowController ofc;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    public CoreService coreService;

    private ApplicationId applicationId;

    @Activate
    public void activate() {
        applicationId = coreService.registerApplication(getClass().getName());
        router_status = new ConcurrentHashMap<>();
        routers = new HashSet<>();
        serials = new HashSet<>();
        log.info("WifiRouterController is on-line");
    }

    @Deactivate
    public void deactivate() {
        applicationId = null;
        router_status = null;
        routers = null;
        serials = null;
        log.info("WifiRouterController is off-line");
    }

    @Override
    public synchronized void set_router_serial(String serial) {
        try {
            serials.add(serial);
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public synchronized void del_router_serial(String serial) {
        try {
            serials.remove(serial);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public synchronized void set_router(Device dev, PortNumber wifi_port) {
        Optional<String> id = get_id_router(dev.id());
        if (!id.isPresent()) {
            routers.add(create_router(dev, wifi_port));
        } else {
            throw new RuntimeException("router already exist");
        }
    }

    @Override
    public synchronized void del_router(String router_id) {
        Optional<WifiCaptiveRouter> router = get_router(router_id);
        try {
            router.ifPresent(r -> {
                routers.remove(router.get());
                if (router_status.get(router_id)) {
                    router_status.remove(r.get_uuid());
                    disconnect_router(router_id);
                }

            });
        } catch (Exception e) {
            throw new RuntimeException("router not found");
        }
    }

    @Override
    public void enable_router(String route_id) {
        Optional<WifiCaptiveRouter> router = get_router(route_id);
        try {
            router.ifPresent(wr -> {
                String serial = wr.get_device().serialNumber();
                boolean status = router_status.get(wr.get_uuid());
                if (!status) {
                    if (check_serial(serial)) {
                        router_status.put(wr.get_uuid(), true);
                    } else {
                        disconnect_router(route_id);
                        throw new RuntimeException("the serial not found");
                    }
                } else {
                    log.warn("the route already is enable");
                }
            });
        } catch (NullPointerException e) {
            log.error("router {} not found", route_id);
        }
    }

    @Override
    public void disable_router(String route_id) {
        Optional<WifiCaptiveRouter> router = get_router(route_id);
        try {
            router.ifPresent(wr -> {
                boolean status = router_status.get(wr.get_uuid());
                if (status) {
                    router_status.replace(wr.get_uuid(), false);
                    disconnect_router(route_id);
                } else {
                    log.warn("route already is disabled");
                }
            });
        } catch (Exception e) {
            log.error("router {} not found", route_id);
        }
    }

    @Override
    public Set<String> get_serials() {
        return ImmutableSet.copyOf(serials);
    }

    @Override
    public Set<WifiCaptiveRouter> get_routers() {
        return ImmutableSet.copyOf(routers);
    }

    @Override
    public Optional<String> get_id_router(DeviceId id) {
        for (WifiCaptiveRouter r : routers) {
            DeviceId dev_id = r.get_device().id();
            if (dev_id.equals(id)) {
                return Optional.of(r.get_uuid());
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<WifiCaptiveRouter> get_router_serial(String serial) {
        return routers.stream()
                      .filter(r -> r.get_device().serialNumber().equals(serial))
                      .findFirst();
    }

    @Override
    public boolean get_router_status(String route_id) {
        if (router_status.containsKey(route_id)) {
            return router_status.get(route_id);
        } else {
            throw new RuntimeException("router not found");
        }
    }


    private Optional<WifiCaptiveRouter> get_router(String router_id) {
        return routers.stream()
                      .filter(r -> r.get_uuid().equals(router_id))
                      .findFirst();
    }

    private boolean check_serial(String serial) {
        return serials.contains(serial);
    }

    private WifiCaptiveRouter create_router(Device dev, PortNumber port) {
        return WifiCaptiveRouter.builder()
                                .setWifiRouterPort(port)
                                .setRouter(dev)
                                .setRouterDescription(Optional.empty())
                                .build();
    }

    private void disconnect_router(String route_id) {
        Optional<WifiCaptiveRouter> router = get_router(route_id);

        try {
            router.ifPresent(r -> {
                Dpid dpid = Dpid.dpid(r.get_device().id().uri());
                OpenFlowSwitch sw = ofc.getSwitch(dpid);
                sw.disconnectSwitch();
            });
        } catch (Exception e) {
            throw new RuntimeException("Switch not found");
        }
    }
}