/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.view;

import br.com.connectanet.sdn.model.WifiCaptiveRouter;
import org.apache.commons.lang3.tuple.Pair;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.PortNumber;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by fernando on 4/5/17.
 */
public interface WifiRouterService {

    void set_router_serial(String serial);

    void del_router_serial(String serial);

    void set_router(Device dev, PortNumber wifi_port);

    void del_router(String router_id);

    void enable_router(String route_id);

    void disable_router(String route_id);

    Set<String> get_serials();

    Optional<String> get_id_router(DeviceId id);

    Set<WifiCaptiveRouter> get_routers();

    boolean get_router_status(String route_id);

    Optional<WifiCaptiveRouter> get_router_serial(String serial);

}
