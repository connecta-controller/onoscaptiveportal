/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.onosproject.net.Device;
import org.onosproject.net.PortNumber;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by fernando on 4/6/17.
 */
public class WifiCaptiveRouter {

    private final String uuid;
    private final Device device;
    private final String description;
    private final PortNumber wport;

    private WifiCaptiveRouter(String uuid, Device device, String description,PortNumber wport) {
        this.uuid = uuid;
        this.wport = wport;
        this.device = device;
        this.description = description;
    }

    public String get_uuid() {
        return uuid;
    }

    public Device get_device() {
        return device;
    }

    public String get_description() {
        return description;
    }

    public static WifiCaptiveRouter.Builder builder(){
        return new Builder();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WifiCaptiveRouter)) return false;
        WifiCaptiveRouter that = (WifiCaptiveRouter) o;
        return Objects.equal(get_uuid(), that.get_uuid()) &&
                Objects.equal(get_device(), that.get_device()) &&
                Objects.equal(get_description(), that.get_description()) &&
                Objects.equal(wport, that.wport);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uuid", uuid)
                .add("device", device)
                .add("description", description)
                .add("wport", wport)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid, device, description, wport);
    }

    public static class Builder {

        private Device device;
        private String description;
        private PortNumber wport;

        public Builder setRouter(Device device){
            this.device = device;
            return this;
        }

        public Builder setWifiRouterPort(PortNumber wport){
            this.wport = wport;
            return this;
        }

        public Builder setRouterDescription(Optional<String> description){
            if (description.isPresent()) {
                this.description = description.get();
            } else {
                this.description = description.orElse("Not avaliable");
            }
            return this;
        }
        public WifiCaptiveRouter build(){
            return new WifiCaptiveRouter(UUID.randomUUID().toString(), device, description, wport );
        }
    }
}
