/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by fernando on 6/23/17.
 */
public enum RouterCommand {
    ADD_SERIAL("add-serial"),
    DEL_SERIAL("del-serial"),
    LIST_SERIAL("list-serial"),
    LIST_ROUTER("list-router");

    private final String command;

    RouterCommand(final String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return command;
    }

    public static List<String> toStringList() {
        return Arrays.stream(values())
                .map(c -> c.toString())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static RouterCommand fromString(String command) {
        Optional<RouterCommand> cmd = Optional.empty();
        if (command != null && !command.isEmpty()) {
            cmd = Arrays.stream(values())
                    .filter(c -> command.equalsIgnoreCase(c.toString()))
                    .findFirst();
        }
        return cmd.get();
    }
}
