/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.controller;

import br.com.connectanet.sdn.exception.AlreadyExistElementException;
import br.com.connectanet.sdn.exception.RoleNotFoundException;
import br.com.connectanet.sdn.exception.UserNotFoundExpception;
import br.com.connectanet.sdn.model.WifiRolePrincipal;
import br.com.connectanet.sdn.model.WifiUserPrincipal;
import br.com.connectanet.sdn.view.WifiUserService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.felix.scr.annotations.*;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;


/**
 * Created by fernando on 4/13/17.
 */
@Component(immediate = true)
@Service
public class WifiUserController implements WifiUserService {

    protected Logger log = LoggerFactory.getLogger(getClass());

    private Optional<WifiRolePrincipal> role_default;

    private List<WifiUserPrincipal> users;
    private List<WifiRolePrincipal> roles;
    private Map<String, String> users_credentials;
    private Map<String, List<String>> users_roles;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    public CoreService coreService;

    private ApplicationId applicationId;

    @Activate
    public void activate() {
        applicationId = coreService.registerApplication(getClass().getName());
        role_default = Optional.empty();
        users = Collections.synchronizedList(new ArrayList<>());
        roles = Collections.synchronizedList(new ArrayList<>());
        users_credentials = Collections.synchronizedMap(new HashMap<>());
        users_roles = Collections.synchronizedMap(new HashMap<>());
        log.info(WifiUserController.class.getCanonicalName() + " is on-line");
    }

    @Deactivate
    public void deactivate() {
        log.info(WifiUserController.class.getSimpleName() + " is off-line");
    }

    @Override
    public void login(String username, String password) {
        Optional<WifiUserPrincipal> u = get_user_name(username);

        if (!u.isPresent()) {
            throw new RuntimeException("user not found");
        }
        u.ifPresent(user -> {
            if (challenger(user.getId(), password)) {
                users.stream()
                        .filter(x -> x.getId().equals(user.getId()))
                        .forEach(x -> {
                            x.setSession(true);
                            x.setLastTime();
                            log.info("user {} has logged in", user.getName());
                        });
            } else {
                throw new RuntimeException("password is wrong ");
            }
        });
    }

    @Override
    public void logout(String username) {
        Optional<WifiUserPrincipal> u = get_user_name(username);
        if (!u.isPresent()) {
            throw new RuntimeException("user not found");
        }
        u.ifPresent(user -> {
            users.stream()
                    .filter(x -> x.getId().equals(user.getId()))
                    .forEach(x -> {
                        x.setSession(false);
                        log.info("user {} has logged out", user.getName());
                    });
        });
    }

    @Override
    public void create_user(String username, String password, Optional<String> description) {
        Optional<WifiUserPrincipal> u = get_user_name(username);
        if (!u.isPresent()) {
            WifiUserPrincipal new_user = new WifiUserPrincipal
                    .Builder()
                    .username(username)
                    .description(description)
                    .build();
            users.add(new_user);
            users_credentials.put(new_user.getId(), cypher_pass(password));
            role_default.ifPresent(role -> {
                add_role(username, role_default.get().getId());
            });
            log.info("New user has included on database");
        } else {
            log.debug("The user already has  included");
            throw new RuntimeException("The user already has been included before");
        }
    }

    @Override
    public List<String> get_user_roles(String user_name) {

        List<String> ret = new ArrayList<>();
        Optional<WifiUserPrincipal> u = get_user_name(user_name);
        u.ifPresent(user -> {
            users_roles.entrySet()
                    .stream()
                    .filter(s -> s.getKey().equals(user.getId()))
                    .map(Map.Entry::getValue)
                    .forEach(id -> {
                        id.forEach(i -> {
                            ret.add(get_role_id(i).get().getName());
                        });
                    });
        });
        return ImmutableList.copyOf(ret);
    }

    @Override
    public void delete_user(String username) {
        Optional<WifiUserPrincipal> u = get_user_name(username);
        u.ifPresent(user -> {
            users.remove(user);
            users_credentials.remove(user.getId());
            users_roles.remove(user.getId());
            log.info("the user was deleted successfully ");
        });
        if (!u.isPresent()) {
            log.debug("user not found");
            throw new RuntimeException("user not found");
        }
    }

    @Override
    public void create_role(String role_name, int priority) {
        if (!check_role_name(role_name)) {
            WifiRolePrincipal new_role = new WifiRolePrincipal
                    .Builder()
                    .setName(role_name)
                    .setPriority(priority)
                    .build();
            roles.add(new_role);
            log.info("Role has created successfully");
        } else {
            log.debug("the role already has  been inserted before");
            throw new RuntimeException("the role already has been inserted before");
        }
    }

    @Override
    public void delete_role(String role_name) {
        Optional<WifiRolePrincipal> r = get_role_name(role_name);
        r.ifPresent(role -> {
            roles.remove(role);
            check_user_role_id(role.getId())
                    .forEach(u -> {
                        rem_role(u.getId(), role.getId());
                    });
            log.info("the role has deleted successfully");
        });
        if (!r.isPresent()) {
            throw new RuntimeException("role is not exist");
        }
    }


    @Override
    public void set_role_default(String role_name) {
        if (check_role_name(role_name)) {
            this.role_default = get_role_name(role_name);
            log.debug("New default role has been set");
        } else {
            throw new RuntimeException("role is not found");
        }
    }

    @Override
    public WifiRolePrincipal get_role_default() {
        return role_default.orElseThrow(RuntimeException::new);
    }

    @Override
    public Map<String, WifiUserPrincipal> get_users() {
        Map<String, WifiUserPrincipal> ret = new HashMap<>();
        users.forEach(u -> {
            ret.put(u.getId(), u);
        });
        return ImmutableMap.copyOf(ret);
    }

    @Override
    public Map<String, WifiRolePrincipal> get_roles() {
        Map<String, WifiRolePrincipal> ret = new HashMap<>();
        roles.forEach(r -> {
            ret.put(r.getId(), r);
        });
        return ImmutableMap.copyOf(ret);
    }

    @Override
    public void add_role(String user_id, String role_id) {
        Optional<WifiUserPrincipal> user = get_user_id(user_id);
        Optional<WifiRolePrincipal> role = get_role_id(role_id);

        if (role.isPresent()) {
            if (user.isPresent()) {
                set_user_role(user_id, role_id);
            } else {
                throw new UserNotFoundExpception("user not found");
            }
        } else {
            throw new RoleNotFoundException("role not found");
        }
    }

    @Override
    public void rem_role(String user_id, String role_id) {
        Optional<WifiUserPrincipal> u = get_user_id(user_id);
        Optional<WifiRolePrincipal> r = get_role_id(role_id);

        if (r.isPresent()) {
            if (u.isPresent()) {
                del_user_role(user_id, r.get().getId());
            } else {
                throw new UserNotFoundExpception("user not found");
            }
        } else {
            throw new RoleNotFoundException("role not found");
        }
    }

    private boolean challenger(String user_id, String password) {
        String user_challenger = cypher_pass(password);
        String user_cypher = users_credentials.get(user_id);
        return user_cypher.equals(user_challenger);
    }

    private Optional<WifiUserPrincipal> get_user_name(String username) {
        return users.stream()
                .filter(u -> u.getName().equalsIgnoreCase(username))
                .findFirst();
    }

    private Optional<WifiUserPrincipal> get_user_id(String id) {
        return users.stream()
                .filter(u -> u.getId().equals(id))
                .findFirst();
    }

    private Optional<WifiRolePrincipal> get_role_name(String role_name) {
        return roles.stream()
                .filter(r -> r.getName().equalsIgnoreCase(role_name))
                .findFirst();
    }

    private Optional<WifiRolePrincipal> get_role_id(String role_id) {
        return roles.stream()
                .filter(r -> r.getId().equals(role_id))
                .findFirst();
    }

    private List<WifiUserPrincipal> check_user_role_name(String role_name) {
        Optional<WifiRolePrincipal> wr = get_role_name(role_name);
        List<WifiUserPrincipal> list = new ArrayList<>();
        wr.ifPresent(role -> {
            users_roles.forEach((u, lr) -> {
                if (lr.contains(role.getId())) {
                    list.add(get_user_id(u).get());
                }
            });
        });
        return ImmutableList.copyOf(list);
    }

    private List<WifiUserPrincipal> check_user_role_id(String role_id) {
        Optional<WifiRolePrincipal> wr = get_role_id(role_id);
        List<WifiUserPrincipal> list = new ArrayList<>();

        users_roles.entrySet()
                .stream()
                .filter(u -> u.getValue().contains(role_id))
                .forEach(u -> {
                    list.add(get_user_id(u.getKey()).get());
                });

        return ImmutableList.copyOf(list);
    }

    private void set_user_role(String user_id, String role_id) {
        users_roles.entrySet()
                .stream()
                .filter(u -> u.getKey().equals(user_id))
                .forEach(u -> {
                    if (!u.getValue().contains(role_id)) {
                        u.getValue().add(role_id);
                    } else {
                        throw new AlreadyExistElementException("role id already included");
                    }
                });
    }

    private void del_user_role(String user_id, String role_id) {
        users_roles.entrySet()
                .stream()
                .filter(u -> u.getKey().equals(user_id))
                .forEach(u -> {
                    if (u.getValue().contains(role_id)) {
                        u.getValue().remove(role_id);
                    } else {
                        throw new RoleNotFoundException("user not contain role");
                    }
                });
    }


    private boolean check_user_id(String user_id) {
        return users.stream()
                .anyMatch(u -> u.getId().equals(user_id));
    }

    private boolean check_user_name(String user_name) {
        return users.stream()
                .anyMatch(u -> u.getName().equalsIgnoreCase(user_name));
    }

    private boolean check_role_id(String role_id) {
        return roles.stream()
                .anyMatch(r -> r.getId().equals(role_id));
    }

    private boolean check_role_name(String role_name) {
        return roles.stream()
                .anyMatch(r -> r.getName().equalsIgnoreCase(role_name));
    }

    private String cypher_pass(String password) {
        return sha256Hex(password.getBytes());
    }

}

