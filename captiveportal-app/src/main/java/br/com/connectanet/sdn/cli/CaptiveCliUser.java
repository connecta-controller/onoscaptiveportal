/*
 * Copyright (c) Connecta Networking 2017 all Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package br.com.connectanet.sdn.cli;

import br.com.connectanet.sdn.view.WifiUserService;
import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.onosproject.cli.AbstractShellCommand;

import java.util.Map;
import java.util.Optional;

/**
 * Created by fernando on 6/23/17.
 */
@Command(scope = "onos", name = "captive-user", description = "Manager user ")
public class CaptiveCliUser extends AbstractShellCommand {

    private static final String DESC_COMMAND =
            "Actions:\n" +
            "--username <name> --password <password> create-user\n" +
            "--username <name> delete-user\n" +
            "--username <name> --rolename <name> set-role \n" +
            "--username <name> --rolename <name> rem-role\n" +
            "list-roles\n" +
            "list-users";



    @Argument(name = "action", description = DESC_COMMAND, required = true)
    private String command = null;

    @Option(name = "--username", aliases = "-u")
    private String username = null;

    @Option(name = "--password", aliases = "-p")
    private String password = null;

    @Option(name = "--rolename", aliases = "-r")
    private String rolename = null;


    @Override
    protected void execute() {
        UserCommand u_cmd = UserCommand.fromString(command);

        switch (u_cmd) {
            case LIST_USERS:
                list_users();
                break;
            case GET_ROLES:
                list_roles();
                break;
            case REM_ROLES:
                rem_role(username,rolename);
                break;
            case SET_ROLES:
                set_role(username,rolename);
                break;
            case LIST_ROLES:
                list_roles();
                break;
            case CREATE_USER:
                create_user(username, password);
                break;
            case DELETE_USER:
                delete_user(username);
                break;
            default:
                error("command not found");

        }
    }

    private void list_users() {
        String exit = "+ id: %s \n+ name: %s \n+ roles: %s  \n+ status: %s";
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);
        user_svc.get_users()
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(user -> {
                    String id = user.getId();
                    String name = user.getName();
                    String role = user_svc.get_user_roles(user.getName()).toString();
                    if (user.isSession()) {
                        print("++++");
                        print(exit, id, name, role, "Connected");
                        print("++++");
                    } else {
                        print("++++");
                        print(exit, id, name, role, "Not Connected");
                        print("++++");
                    }
                });
    }

    private void create_user(String user_name, String password) {
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);
        try {
            user_svc.create_user(user_name, password, Optional.empty());
            print("user has inserted with successfully ");
        } catch (Exception e) {
            print("ERROR: " + e.getMessage());
            print(DESC_COMMAND);
        }
    }

    private void delete_user(String user_name) {
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);
        try {
            user_svc.delete_user(user_name);
            print("user has deleted with successfully");
        } catch (Exception e) {
            print("ERROR: " + e.getMessage());
        }
    }

    private void list_roles(){
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);

        String exit = "id: %s  name: %s";

        user_svc.get_roles()
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(role -> {
                    print(exit, role.getId(), role.getName());
                });

    }

    private void set_role(String user_name, String role_name){
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);
        try {
            user_svc.add_role(user_name,role_name);
            print("role has added with successufully to %s", user_name);
        } catch (RuntimeException e) {
            print("ERROR: " + e.getMessage());
        }


    }

    private void rem_role(String user_name, String role_name){
        WifiUserService user_svc = AbstractShellCommand.get(WifiUserService.class);
        try {
            user_svc.rem_role(user_name, role_name);
            print("role has added with successufully to %s", user_name);
        } catch (RuntimeException e) {
            print("ERROR: " + e.getMessage());
        }
    }
}
